import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Event from '../views/Event.vue'
import Profile from '../views/Profile'
import Auth from '../views/Auth'
import User from '../views/User'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/event/:event_id',
    name: 'event',
    component: Event,
    props: true
  },
  {
    path: '/profile/',
    name: 'profile',
    component: Profile
  },
  {
    path: '/auth/',
    name: 'auth',
    component: Auth,
  },
  {
    path: '/register/',
    name: 'register',
    component: Auth,
    props: { register: true },
  },
  {
    path: '/user/',
    name: 'user',
    component: User,
    props: true
  },
  {
    path: '/admin/',
    name: 'admin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "admin" */ '../views/AdminPanel.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
