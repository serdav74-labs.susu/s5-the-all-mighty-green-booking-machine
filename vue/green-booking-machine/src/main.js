import Vue from 'vue'
import App from './App.vue'
import router from './router'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false

// hostname for API requests. set empty to use the same host
Vue.prototype.$hostname = Vue.config.productionTip ? '' : 'http://localhost:8080';

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
