import os
from pony.orm import *
from datetime import datetime
from pydantic import BaseModel

db = Database()


class PNewUser(BaseModel):
    email: str = ""
    username: str = ""
    password: str = ""


class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    email = Optional(str)
    username = Required(str)
    password = Required(str)
    token = Required(str)
    bookings = Set('Booking')

    def to_dict(self):
        return {"id": self.id, "email": self.email, "username": self.username, "access_token": self.token}


class PEvent(BaseModel):
    name: str
    description: str
    image_url: str = ""
    start: datetime
    latitude: float = None
    longitude: float = None


class Event(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    description = Required(str)
    image_url = Optional(str)
    start = Required(datetime)
    options = Set('EventOption')
    latitude = Optional(float)
    longitude = Optional(float)

    def to_dict(self):
        return {"id": self.id, "name": self.name, "description": self.description, "image_url": self.image_url,
                "start": self.start, "options": list(obj.to_dict() for obj in self.options),
                "latitude": self.latitude, "longitude": self.longitude}


class PEventOption(BaseModel):
    name: str
    description: str = ""
    capacity: int
    price: float


class EventOption(db.Entity):
    id = PrimaryKey(int, auto=True)
    event = Required(Event)
    name = Required(str)
    description = Optional(str)
    capacity = Required(int)
    bookings = Set('Booking')
    price = Required(float)

    def to_dict(self):
        return {"id": self.id, "event": self.event, "name": self.name, "description": self.description,
                "capacity": self.capacity, "bookings": len(list(obj.to_dict() for obj in self.bookings)),
                "price": self.price}


class Booking(db.Entity):
    id = PrimaryKey(int, auto=True)
    user = Required(User)
    event_option = Required(EventOption)
    created_at = Required(datetime)
    tickets = Required(int)

    def to_dict(self):
        return {"id": self.id, "user": self.user, "event_option": self.event_option,
                "created_at": self.created_at, "tickets": self.tickets}


def bind_db(db, host='db'):
    user = os.environ.get('POSTGRES_USER')
    password = os.environ.get('POSTGRES_PASSWORD')
    db_name = os.environ.get('POSTGRES_DB')
    print(user, password, db_name)
    db.bind(provider='postgres', host=host, user=user, password=password, database=db_name)
    db.generate_mapping(create_tables=True)


bind_db(db)
