import uuid
from datetime import datetime
from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext
from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.responses import FileResponse
from starlette.middleware.cors import CORSMiddleware
from pony.orm import db_session, ObjectNotFound, select, commit
from db import Event, PEvent, EventOption, PEventOption, User, PNewUser, Booking

# if you change variable name, don't forget to edit Dockerfile command
app = FastAPI()
api = FastAPI()
frontend = Starlette()


@frontend.middleware("http")
async def add_custom_header(request, call_next):
    response = await call_next(request)
    if response.status_code == 404:
        return FileResponse('spa/index.html')
    return response


# CORS
origins = [
    "http://localhost:80",
    "http://localhost:8080",
    "http://localhost:8081",
    "http://localhost:8082",
    "http://192.168.1.198:8082",
    "http://rabpujia.ddns.net"
    ]

app.add_middleware(CORSMiddleware, allow_origins=origins, allow_credentials=True, allow_methods=["*"],
                   allow_headers=["*"])

# Authentication
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(password):
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def decode_token(token):
    with db_session:
        user = User.get(token=token)
        return user


def get_current_user(token: str = Depends(oauth2_scheme)):
    user = decode_token(token)
    return user


def get_admin(user: User = Depends(get_current_user)):
    if user.username == "admin":
        return user
    else:
        raise HTTPException(status_code=403, detail="Нет доступа.")


def create_access_token():
    return str(uuid.uuid4())


@api.post('/token/')
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    with db_session:
        user = User.get(username=form_data.username)
        if not user or not verify_password(form_data.password, user.password):
            raise HTTPException(status_code=400, detail="Неправильное имя пользователя или пароль.")
        user.token = create_access_token()
        return {"access_token": user.token, "token_type": "bearer"}


@api.post('/user/register/')
def register(new_user: PNewUser):
    with db_session:
        if new_user.username == "" or new_user.password == "":
            raise HTTPException(status_code=400, detail="Имя пользователя и/или пароль не могут быть пустыми!")
        if User.get(username=new_user.username):
            raise HTTPException(status_code=400, detail="Такое имя пользователя уже занято.")
        user = User(email=new_user.email, username=new_user.username, password=pwd_context.hash(new_user.password),
                    token=create_access_token())
        commit()
        return user.to_dict()


# CRUD events
@api.get('/event/')
def get_all_events():
    with db_session:
        query = select(event for event in Event)
        output = list()
        for event in query:
            output.append(event.to_dict())
        return output


@api.get('/event/{id}/')
def get_event_by_id(id: int):
    with db_session:
        try:
            event = Event[id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        return event.to_dict()


@api.get('/event_list/featured/')
def get_featured_events():
    with db_session:
        query = Event.select().random(5)
        output = list()
        for event in query:
            output.append(event.to_dict())
        return output


@api.get('/event/{id}/similar/')
def get_similar_events(id: int):
    with db_session:
        query = Event.select().random(5)
        output = list()
        for event in query:
            output.append(event.to_dict())
        return output


@api.post('/event/')
def create_event(event: PEvent, user: User = Depends(get_admin)):
    with db_session:
        new_event = Event(name=event.name, description=event.description, image_url=event.image_url,
                          start=event.start, latitude=event.latitude, longitude=event.longitude)
        commit()
        return new_event.to_dict()


@api.delete('/event/{id}/')
def remove_event(id: int, user: User = Depends(get_admin)):
    with db_session:
        try:
            event = Event[id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        if event.options.is_empty():
            event.delete()
            return {"message": "success"}
        else:
            raise HTTPException(status_code=409, detail="Поле 'options' не пустое.")


@api.put('/event/{id}/')
def update_event(updated_event: PEvent, id: int, user: User = Depends(get_admin)):
    with db_session:
        event = Event[id]
        event.name = updated_event.name
        event.description = updated_event.description
        event.image_url = updated_event.image_url
        event.start = updated_event.start
        event.latitude = updated_event.latitude
        event.longitude = updated_event.longitude
        commit()
        return event.to_dict()


@api.get('/user/booked_events/')
def get_booked_events(user: User = Depends(get_current_user)):
    with db_session:
        bookings = select(obj for obj in user.bookings)
        event_options = select(obj.event_option for obj in bookings)
        events = select(obj.event for obj in event_options)
        booked_events = [event.to_dict() for event in events]
        return booked_events


# CRUD options
@api.get('/event/{event_id}/option/{option_id}/')
def get_option_data(event_id: int, option_id: int):
    with db_session:
        try:
            option = EventOption[option_id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        return option.to_dict()


@api.post('/event/{event_id}/option/')
def create_option(option: PEventOption, event_id: int, user: User = Depends(get_admin)):
    with db_session:
        try:
            event = Event[event_id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        option = EventOption(event=event, name=option.name, description=option.description, capacity=option.capacity,
                             price=option.price)
        commit()
        return option.to_dict()


@api.delete('/event/{event_id}/option/{option_id}/')
def remove_option(event_id: int, option_id: int, user: User = Depends(get_admin)):
    with db_session:
        try:
            option = EventOption[option_id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        if option.bookings.is_empty():
            option.delete()
            return {"message": "success"}
        else:
            raise HTTPException(status_code=409, detail="Поле 'bookings' не пустое.")


@api.put('/event/{event_id}/option/{option_id}/')
def update_option(updated_option: PEventOption, event_id: int, option_id: int, user: User = Depends(get_admin)):
    with db_session:
        option = EventOption[option_id]
        option.name = updated_option.name
        option.description = updated_option.description
        option.capacity = updated_option.capacity
        option.price = updated_option.price
        commit()
        return option.to_dict()


# CRUD users
@api.get('/user/')
def get_user_info(user: User = Depends(get_current_user)):
    return user.to_dict()


# CRUD bookings
@api.get('/booking/')
def get_users_bookings(user: User = Depends(get_current_user)):
    with db_session:
        objects = select(booking for booking in user.bookings)
        bookings = [obj.to_dict() for obj in objects]
        return bookings


@api.post('/event/{event_id}/book/')
def create_booking(event_id: int, bookings: list, user: User = Depends(get_current_user)):
    with db_session:
        if len(bookings) == 0:
            raise HTTPException(status_code=400, detail="Вы ничего не заказали.")
        created_bookings = list()
        for pair in bookings:
            id = pair["id"]
            value = pair["value"]
            if value < 1:
                raise HTTPException(status_code=400, detail="Некорректное количество билетов.")
            try:
                option = EventOption[id]
            except ObjectNotFound:
                raise HTTPException(status_code=404, detail="Объект не найден.")
            booking = Booking(user=user.id, event_option=option, created_at=datetime.now(), tickets=value)
            created_bookings.append(booking.to_dict())
        commit()
        return created_bookings


@api.delete('/booking/{booking_id}/')
def remove_booking(booking_id: int, user: User = Depends(get_current_user)):
    with db_session:
        try:
            booking = Booking[booking_id]
        except ObjectNotFound:
            raise HTTPException(status_code=404, detail="Объект не найден.")
        users_bookings = select(booking for booking in user.bookings)
        if booking not in users_bookings:
            raise HTTPException(status_code=403, detail="Нет доступа.")
        if datetime.now() > booking.event_option.event.start:
            raise HTTPException(status_code=400, detail="Мероприятие уже окончено.")
        booking.delete()
        return {"message": "success"}


frontend.mount('/', StaticFiles(directory='spa', html=True))
app.mount('/api', app=api)
app.mount('/', app=frontend)
