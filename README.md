# The All-Mighty Green Booking Machine

## Installation

1. Clone this repo
1. Ensure you have `docker` and `docker-compose` installed on your machine
1. `cp .env.dist .env`
1. Put your values in `.env` if needed
1. Follow *Updating* section

## Updating

1. Ensure you have fresh `.env` file
1. Rebuild project with `docker-compose build`

## Launching

* `docker-compose up -d` to start and detach
* `docker-compose down` to shut everything down
* Avoid using `docker-compose restart` because it restarts every container separately

## Troubleshooting

*Q:* Web app couldn't connect to my database on first launch!

*A:* Wait for MariaDB to initialize, then shut everything down and start again. If it doesn't help, check your `.env` file.

---
