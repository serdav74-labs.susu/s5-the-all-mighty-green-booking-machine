# Building Vue SPA
FROM node:lts-alpine as build-stage
WORKDIR /app/vue/green-booking-machine
COPY vue/green-booking-machine/package*.json ./
RUN npm install

COPY vue/green-booking-machine ./
RUN npm run dev_build


FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7-alpine3.8 as serve-stage
# copying built Vue app to our FastAPI server

# required to install pypi psycopg2 library
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev autoconf automake g++ make

WORKDIR /app/web
COPY web/requirements.txt ./
RUN pip install -r requirements.txt
COPY web .
COPY --from=build-stage /app/vue/green-booking-machine/dist ./spa
CMD sleep 15s && uvicorn app:app --host 0.0.0.0 --port 80

